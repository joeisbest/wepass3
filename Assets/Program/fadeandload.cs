﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class fadeandload : MonoBehaviour {


	private bool inputenable=true;
	private Animation anim;

	public  int id=0;

	public GameObject[] items;
	public GameObject[] woods;
	private Vector3 origintran;

	void Awake(){
		//Time.timeScale = 1;
		//Instantiate (woods[0],items[id].GetComponent<Transform>().transform.position,items[id].GetComponent<Transform>().transform.rotation);
		woods[0].transform.position=items[0].GetComponent<Transform>().transform.position;
		origintran = woods [1].transform.position;
	}


	void Start(){
		anim = GetComponent<Animation> ();
		anim.Play ("fade in");
	}


	void Update () {
		InputControl ();

	}


	public void selectAnimPlay(int id_){
			//Instantiate (woods[id] ,items[id].GetComponent<Transform>().transform.position,items[id].GetComponent<Transform>().transform.rotation);
              woods[id_].transform.position=items[id_].GetComponent<Transform>().transform.position;
		switch(id_){
		case 0:
			woods [1].transform.position = origintran;
			woods [2].transform.position = origintran;
			break;
		case 1:
			woods [0].transform.position = origintran;
			woods [2].transform.position = origintran;
			break;
		case 2:
			woods [0].transform.position = origintran;
			woods [1].transform.position = origintran;
			break;
		}
	  }

	void InputControl(){
		if (inputenable) {
			if (Input.GetKeyDown (KeyCode.DownArrow)) {
				id++;
				id = Mathf.Clamp (id, 0, 2);
				selectAnimPlay (id);

			}
			if (Input.GetKeyDown (KeyCode.UpArrow)) {
				id--;
				id = Mathf.Clamp (id, 0, 2);
				selectAnimPlay (id);

			}
			//選項控制
			if (id == 0) {
				if (Input.GetKeyDown (KeyCode.Return) && !anim.IsPlaying ("fade in")) {
					inputenable = false;
					woods [id].GetComponent<Animator> ().speed = 0;
					anim.Play ("fade out");
				}
			}
			if (id == 2) {
				if (Input.GetKeyDown (KeyCode.Return) && !anim.IsPlaying ("fade in")) {
					inputenable = false;
					woods [id].GetComponent<Animator> ().speed = 0;
					anim.Play ("fade out");

				}
			}
		}
	}

	public void mapload (string map) {
		if (id == 0) {
			SceneManager.LoadScene (map);
		}
		if (id == 2) {
			Application.Quit ();
		}
	}	

	/*public void mapquit () {
		if(id==2)
				Application.Quit();
	}	*/



}
