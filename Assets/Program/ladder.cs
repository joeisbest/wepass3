﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ladder : MonoBehaviour {
	
	public float speed=2;
	public bool isClimbing=false;

	public GameObject player;
	private Rigidbody2D playerRigi;


	void Start () {
		playerRigi = player.GetComponent<Rigidbody2D> ();
	}
		

	void OnTriggerStay2D(Collider2D other){
		climbTrigger (other);
	}


	void climbTrigger(Collider2D tplay){

		if (tplay.tag == "Player" && Input.GetKeyDown (KeyCode.UpArrow)) {
			isClimbing = true;
			playerRigi.gravityScale = 0;
			//判斷
			  if (isClimbing && Input.GetKey (KeyCode.UpArrow)) {
			    	playerRigi.velocity = new Vector2 (0, speed);
			  } 
			  else if (isClimbing && Input.GetKey (KeyCode.DownArrow)) {
			 	    playerRigi.velocity = new Vector2 (0, -speed);
			  } 
			  else {
				    playerRigi.velocity = new Vector2 (0, 0);
			 }
		}
	}



	void OnTriggerExit2D(Collider2D other){
		playerRigi.gravityScale = 1;
	}

}