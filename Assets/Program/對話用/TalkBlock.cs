﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class TalkBlock : MonoBehaviour{
	
	public Flowchart talkTip01;

	private Player attr_player;
	private Animation anim;

	public string onCollosionEnter; //block名字
	public bool Choose_Isdestoy;
	public bool Choose_Isinput;


	void Start(){
		attr_player = GameObject.Find("Player").GetComponent<Player>(); //把玩家物件從其他廠警抓來
		anim = GameObject.Find ("fade block").GetComponent<Animation> (); //把淡入淡出物件抓來，以進行控制權判斷
	}

	void Update(){
		if (!talkTip01.HasExecutingBlocks() && !anim.isPlaying) {
			attr_player.controlEnable = true;
			attr_player.isTalking = false;
	 	  }

		}

	void OnTriggerEnter2D(Collider2D other){
		Block targetBlock = talkTip01.FindBlock (onCollosionEnter);
		if (!Choose_Isinput) {	
			if (other.gameObject.CompareTag ("Player")) {
				talkTip01.ExecuteBlock (targetBlock);
				//對話暫停
				attr_player.isTalking = true; //使對話不能暫停

				attr_player.controlEnable = false;
				attr_player.anim.SetBool ("run", false);
			}
		}

	}
		
	void OnTriggerStay2D(Collider2D other){

		Block targetBlock = talkTip01.FindBlock (onCollosionEnter);
		 if (other.gameObject.CompareTag("Player") ) {

			if (Choose_Isinput) {	
				if (Input.GetKeyDown (KeyCode.Space)) {
					talkTip01.ExecuteBlock (targetBlock);
					//對話暫停
					attr_player.isTalking = true; 

					attr_player.controlEnable = false;
					attr_player.anim.SetBool ("run", false);
				}
			}
     	}
	}

	void OnTriggerExit2D(Collider2D other){
		if (Choose_Isdestoy)
			Destroy (gameObject);
	}
		



}
