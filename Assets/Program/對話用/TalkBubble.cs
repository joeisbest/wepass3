﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TalkBubble : MonoBehaviour{

	private bool D_d=false;//破壞複製框判斷
	private string thisName;


	public enum Talk_Condition{
	   Tips_UI,
		Talk_UI,
		Think_UI,
		Standby_WD,
		Touchtip_WD
	}
	public enum Talk_ID{
	     A,
	     B,
		 C
	}

	[Header("狀態機")]
	public Talk_Condition condition_ID;
	public bool Choose_Isdestoy;

	[Header("文本")]
	public string talk_content;
	public string talk_content2;

	[Header("Talk_UI使用")]
	public Talk_ID ID;

	public  UILabel talk_temp1;
	public  UILabel talk_temp2;
	public  UILabel talk_temp3;

	public  GameObject talk_sprite;
	private GameObject talk_spritein;

	//置換用座標
	private  Vector3 wrpos; //轉換前"世界:座標"
	private Vector3 pos;  //轉換後"世界:攝影機座標"
	private Vector3 pos2; //使用pos轉換後"ui世界:座標"
	//使用攝影機座標(世界→ui)做通道進行置換,得出相對於世界的ui座標

	[Header("Standby_WD使用")]
	public GameObject standby_temp;

	//複製
	private GameObject standby_tempin;

	[Header("Touchtip_WD使用")]
	public GameObject touchtip_temp;
	public GameObject touchtip_key;

	//複製
	private GameObject touchtip_tempin;
	private GameObject touchtip_keyin;

	[Header("Tips_UI使用")]
	public GameObject tip_temp;
	public UILabel tip_text;

	public GameObject[] tip_key;
	//複製
	private GameObject tip_tempin;
	public GameObject[] tip_keyin;

	[Header("框位置")]
	public GameObject headCube;
	private Vector3 headCube_po;

	[Header("接收對話碰撞框")]
	public GameObject block_trigger;



	void Start () {
		tip_keyin = new GameObject[tip_key.Length];

		wrpos=headCube.transform.position;
		headCube_po = headCube.transform.position;

		thisName = this.name;//取得名字
	}

	void Update(){
		Destroy_intial ();
	}


	void Destroy_intial(){
		if (D_d) {
			for (int i = 0; i < 10; i++) {
				Destroy (GameObject.Find (thisName + "框複製" + i + "型"));
				//Debug.Log (thisName + "框複製" + i + "型");
			}
			if (Choose_Isdestoy) {
				Destroy (gameObject);
			}
		}
	}



        void OnTriggerStay2D(Collider2D other){

		 if (other.gameObject.CompareTag ("Player")) {
			
			D_d = false;
			//轉換座標
			pos = Camera.main.WorldToScreenPoint (wrpos);
			pos.z = 0;
			pos2 = UICamera.currentCamera.ScreenToWorldPoint (pos); 
//=======================================================
			if (condition_ID == Talk_Condition.Talk_UI) {
				if (talk_spritein == null) {
					talk_spritein = Instantiate (talk_sprite, new Vector3 (headCube_po.x, headCube_po.y, 0), headCube.transform.rotation);
					talk_spritein.name=thisName+"框複製0型";
					SceneManager.MoveGameObjectToScene (talk_spritein, gameObject.scene); // 將複製物件放到氣泡框存在的scene
				}

				switch (ID) {
				case Talk_ID.A:
					talk_temp1.transform.position = pos2;
					talk_temp1.text = talk_content;
					break;
				case Talk_ID.B:	
					talk_temp2.transform.position = pos2;
					talk_temp2.text = talk_content2;
					break;
				case Talk_ID.C:
					talk_temp3.transform.position = pos2;
					break;
				}
			
			}
//===========================================
			if (condition_ID == Talk_Condition.Standby_WD) {
				if (standby_tempin == null) {
					standby_tempin = Instantiate (standby_temp, new Vector3 (headCube_po.x, headCube_po.y, 0), headCube.transform.rotation);
					standby_tempin.name=thisName+"框複製1型";
					SceneManager.MoveGameObjectToScene (standby_tempin, gameObject.scene); // 將複製物件放到氣泡框存在的scene...
				}
			}
//===========================================
			if (condition_ID == Talk_Condition.Touchtip_WD) {
				if (touchtip_tempin == null) {
					touchtip_tempin = Instantiate (touchtip_temp, new Vector3 (headCube_po.x, headCube_po.y, 0), headCube.transform.rotation);
					touchtip_tempin.name=thisName+"框複製2型";
					SceneManager.MoveGameObjectToScene (touchtip_tempin, gameObject.scene);

					touchtip_keyin = Instantiate (touchtip_key, new Vector3 (headCube_po.x, headCube_po.y, 0), headCube.transform.rotation);
					touchtip_keyin.name=thisName+"框複製3型";
					SceneManager.MoveGameObjectToScene (touchtip_keyin, gameObject.scene);
				}
			}
			//===========================================
			if (condition_ID == Talk_Condition.Tips_UI) {
				if (tip_tempin == null) {
					tip_tempin = Instantiate (tip_temp, new Vector3 (headCube_po.x, headCube_po.y, 0), headCube.transform.rotation);
					tip_tempin.name=thisName+"框複製4型";
					SceneManager.MoveGameObjectToScene (tip_tempin, gameObject.scene);

					for (int i = 0; i < tip_key.Length; i++) {
						tip_keyin [i] = Instantiate (tip_key [i], new Vector3 (headCube_po.x - (i * 0.7f), headCube_po.y + 0.4f, 0), headCube.transform.rotation);
						tip_keyin [i].name=thisName+"框複製"+(5+i)+"型";
						SceneManager.MoveGameObjectToScene (tip_keyin [i], gameObject.scene);
					}
				}
					tip_text.transform.position = pos2;
					tip_text.text = talk_content;

				
			}
	  	}

	}


	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.CompareTag ("Player")) {
			D_d = true;
			if (condition_ID == Talk_Condition.Tips_UI) {
				tip_text.transform.position = Vector3.zero;
			}
				
				//
			if (condition_ID == Talk_Condition.Talk_UI) {
				switch (ID) {
				case Talk_ID.A:
					talk_temp1.transform.position = Vector3.zero;
					break;
				case Talk_ID.B:	
					talk_temp2.transform.position = Vector3.zero;
					break;
				case Talk_ID.C:
					talk_temp3.transform.position = Vector3.zero;
					break;
				}
			}
		
		}

}

}