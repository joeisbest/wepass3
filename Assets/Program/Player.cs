﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	[Header("暫停面板")]
	public GameObject pausemenu;

	Rigidbody2D playerRigidbody2D;

	//public bool inputEnable=true;
	[Header("狀態值")]
	public bool controlEnable=true;
	public bool isTalking=false;
	public bool ispause=false;

	[Header("走路速度")]
	[Range(0,150)]
	public float speedX;

	float speedY;
	public Animator anim;

    [Header("重質向上推力")]
    public float yForce;

    public bool Jumpkey {//使用於判斷bool的控制式會很好用
        get {
			return Input.GetKeyDown(KeyCode.Z);
        }

    }
	[Header("感應地板的距離")]
	[Range(0, 0.5f)]
	public float distance;

	[Header("感應地板的射線")]
	public Transform groundCheck;

	[Header("地面圖層")]
	public LayerMask groundLayer;

	public bool grounded;
	bool IsGround {
		get{
			Vector2 start = groundCheck.position;     //物件的位置
			Vector2 end = new Vector2(start.x, start.y - distance);  //距離物件的結束點

			Debug.DrawLine(start, end, Color.blue);//劃出虛構的射線
			grounded = Physics2D.Linecast(start, end, groundLayer); //判斷實際存在的射線

			return grounded;
		}
	}



    void Start(){
        playerRigidbody2D = GetComponent<Rigidbody2D>();   
		anim=GetComponentInChildren<Animator>();

    }


	void Update(){
		Pause ();
	}

	void FixedUpdate () {
		controlmanager ();
		//animatorManager();

	}
		
	void SetControl(){
			Movement ();
			TryJump ();

	}
		

	//分隔線

	void TryJump()
	{
		if (IsGround && Jumpkey)
		{
			playerRigidbody2D.AddForce(Vector2.up * yForce);
			//anim
		}
	}
		
    void Movement(){
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.rotation = Quaternion.Euler (0, 180, 0);
			transform.Translate (Vector2.left * speedX *Time.deltaTime, Space.World);
		}
		else if (Input.GetKey (KeyCode.RightArrow)) {
			transform.rotation = Quaternion.Euler (0, 0, 0);
			transform.Translate (-Vector2.left * speedX * Time.deltaTime, Space.World);
		}
		else {
		}
    }

	void Pause(){
		//鎖定畫面+控制
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (!ispause && !isTalking) {
				controlEnable = false;
				Time.timeScale = 0;
				ispause = true;
				//暫停面板
				pausemenu.SetActive (true);
			}
			else {
				controlEnable = true;
				Time.timeScale = 1;
				ispause = false;
				//暫停面板
				pausemenu.GetComponent<pauseMenu>().id=0;
				pausemenu.SetActive (false);
			}
		}
		//鎖定控制	
	}

	void controlmanager(){
		if (controlEnable) {
			SetControl ();
			animatorManager ();
		}
		else {
			}
	
	}


	void animatorManager(){
		//跳躍
		if (grounded) {
			anim.SetBool ("isjump", false);
		} else {
			anim.SetBool("isjump",true); 
		}
		//走路
		if (Input.GetKey (KeyCode.LeftArrow) && grounded) {
			anim.SetBool ("run", true);
		}
		else if (Input.GetKey (KeyCode.RightArrow) && grounded) {
			anim.SetBool ("run", true);
		}
		else {
			anim.SetBool ("run", false);
		}
	
	}

	


}
